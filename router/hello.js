var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

const Category = mongoose.model("Category", {
	name: String,
	description: String,
	position: Number,
	createDate: Date,
});

router.get("/", function (req, res) {
	res.send("Hello");

	new Category({
		name: "Улс төр",
		position: 1,
		createDate: new Date(),
	}).save();
});

router.get("/message", function (req, res) {
	res.send("Message");
});

module.exports = router;
