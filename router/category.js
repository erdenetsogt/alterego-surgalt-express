var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

router.get("/list", async function (req, res) {
	const Category = mongoose.model("Category");
	const list = await Category.find().sort("name");
	res.json(list);
});

router.post("/create", express.json(), async function (req, res) {
	const Category = mongoose.model("Category");
	await new Category({
		name: req.body.name,
		createDate: new Date(),
	}).save();

	res.json({ status: "success" });
});

router.post("/update", express.json(), async function (req, res) {
	const Category = mongoose.model("Category");

	await Category.findByIdAndUpdate(req.body._id, {
		name: req.body.name,
	});

	res.json({ status: "success" });
});

router.post("/delete", express.json(), async function (req, res) {
	const Category = mongoose.model("Category");

	await Category.findByIdAndDelete(req.body._id);

	res.json({ status: "success" });
});

module.exports = router;
