const express = require("express");
const app = express();
const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://admin:nklkQ50fknoTf2Mk@cluster0.unj3f.mongodb.net/blog?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true,
});
mongoose.set("debug", true);

app.use("/hello", require("./router/hello"));
app.use("/api/category", require("./router/category"));

app.listen(4000);
